from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .models import Event, Task, Comment, Schedule, Notification, UserProfile
from .forms import EventForm, TaskForm, CommentForm, UserProfileForm, NotificationForm, ScheduleForm
from django.contrib.auth import views as auth_views

# Главная страница
def index(request):
    return render(request, 'stations/index.html')

# Список событий
@login_required
def event_list(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    organized_events = Event.objects.filter(organizer=user_profile)
    participating_events = user_profile.events_attending.all()
    return render(request, 'stations/event_list.html', {
        'organized_events': organized_events,
        'participating_events': participating_events
    })

# Просмотр деталей события
@login_required
def event_detail(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    comments = Comment.objects.filter(event=event)
    return render(request, 'stations/event_detail.html', {'event': event, 'comments': comments})

# Создание нового события
@login_required
def event_create(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            event = form.save(commit=False)
            event.organizer = request.user.userprofile  # Присваиваем организатора
            event.save()
            form.save_m2m()  # Сохраняем связанные данные ManyToMany

            # Добавление события в расписание организатора
            schedule, created = Schedule.objects.get_or_create(user=request.user.userprofile)
            schedule.events.add(event)

            # Добавление события в расписания всех участников
            for participant in event.participants.all():
                schedule, created = Schedule.objects.get_or_create(user=participant)
                schedule.events.add(event)

            return redirect('event_list')
    else:
        form = EventForm()
    return render(request, 'stations/event_form.html', {'form': form, 'action': 'Create'})


# Редактирование события
@login_required
def event_edit(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            form.save()
            # Обновление расписаний всех участников
            all_participants = set(event.participants.all())
            previous_participants = set(Schedule.objects.filter(events=event).values_list('user', flat=True))
            new_participants = all_participants - previous_participants
            removed_participants = previous_participants - all_participants

            # Добавляем новых участников
            for participant in new_participants:
                schedule, created = Schedule.objects.get_or_create(user=participant)
                schedule.events.add(event)

            # Удаляем событие у участников, которые были удалены
            for participant in removed_participants:
                schedule = Schedule.objects.get(user=participant)
                schedule.events.remove(event)

            return redirect('event_list')
    else:
        form = EventForm(instance=event)
    return render(request, 'stations/event_form.html', {'form': form, 'action': 'Edit'})


# Удаление события
@login_required
def event_delete(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        event.delete()
        return redirect('event_list')
    return render(request, 'stations/event_confirm_delete.html', {'event': event})

# Список задач
@login_required
def task_list(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    assigned_tasks = user_profile.tasks.all()
    return render(request, 'stations/task_list.html', {
        'assigned_tasks': assigned_tasks
    })

# Просмотр деталей задачи
@login_required
def task_detail(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    comments = Comment.objects.filter(task=task)
    return render(request, 'stations/task_detail.html', {'task': task, 'comments': comments})

# Создание новой задачи
@login_required
def task_create(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('task_list')
    else:
        form = TaskForm()
    return render(request, 'stations/task_form.html', {'form': form, 'action': 'Create'})

# Редактирование задачи
@login_required
def task_edit(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('task_list')
    else:
        form = TaskForm(instance=task)
    return render(request, 'stations/task_form.html', {'form': form, 'action': 'Edit'})

# Удаление задачи
@login_required
def task_delete(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST':
        task.delete()
        return redirect('task_list')
    return render(request, 'stations/task_confirm_delete.html', {'task': task})

# Профиль пользователя
@login_required
def profile(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    tasks = user_profile.tasks.all()
    events = user_profile.events_attending.all()
    # Получите данные пользователя из связанной модели User
    first_name = user_profile.user.first_name
    last_name = user_profile.user.last_name
    email = user_profile.user.email
    # Теперь передайте эти данные в шаблон
    return render(request, 'stations/profile.html', {
        'user_profile': user_profile,
        'tasks': tasks,
        'events': events,
        'first_name': first_name,
        'last_name': last_name,
        'email': email
    })


# Редактирование профиля пользователя
@login_required
def profile_edit(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    if request.method == 'POST':
        form = UserProfileForm(request.POST, instance=user_profile)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = UserProfileForm(instance=user_profile)
    return render(request, 'stations/profile_form.html', {'form': form, 'action': 'Edit'})

# Просмотр расписания
@login_required
def schedule_view(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    schedule = get_object_or_404(Schedule, user=user_profile)
    events = schedule.events.order_by('start_time')

    events_with_roles = []
    for event in events:
        role = 'Organizer' if event.organizer == user_profile else 'Participant'
        events_with_roles.append({'event': event, 'role': role})

    return render(request, 'stations/schedule.html', {
        'schedule': schedule,
        'events_with_roles': events_with_roles
    })

# Уведомления пользователя
@login_required
def notification_list(request):
    notifications = Notification.objects.filter(user__user=request.user)
    return render(request, 'stations/notification_list.html', {'notifications': notifications})

class CustomLoginView(auth_views.LoginView):
    def form_valid(self, form):
        messages.success(self.request, f'Welcome, {form.get_user().username}!')
        return super().form_valid(form)

class CustomLogoutView(auth_views.LogoutView):
    def dispatch(self, request, *args, **kwargs):
        messages.success(request, 'You have been logged out successfully!')
        return super().dispatch(request, *args, **kwargs)
