from django.contrib import admin
from .models import UserProfile, Event, Task, Comment, Schedule, Notification
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import UserProfile

# Register your models here.

admin.site.register(UserProfile)
admin.site.register(Event)
admin.site.register(Task)
admin.site.register(Comment)
admin.site.register(Schedule)
admin.site.register(Notification)

# Импортируем InlineModelAdmin для включения UserProfile в форму User
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'

# Наследуемся от UserAdmin для кастомизации
class CustomUserAdmin(UserAdmin):
    inlines = (UserProfileInline, )
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'get_role', 'get_phone')

    def get_role(self, instance):
        return instance.userprofile.role
    get_role.short_description = 'Role'

    def get_phone(self, instance):
        return instance.userprofile.phone_number
    get_phone.short_description = 'Phone Number'

    # Обновление методов сохранения и удаления для работы с UserProfile
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not hasattr(obj, 'userprofile'):
            UserProfile.objects.create(user=obj)
        else:
            obj.userprofile.save()

# Перерегистрируем модель User
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
