from django import forms
from django.forms import DateTimeInput

from .models import Event, Task, Comment, UserProfile, Notification, Schedule


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'description', 'start_time', 'end_time', 'participants']
        widgets = {
            'start_time': DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
            'end_time': DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
        }

    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['start_time'].input_formats = ('%Y-%m-%dT%H:%M',)
        self.fields['end_time'].input_formats = ('%Y-%m-%dT%H:%M',)

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description', 'due_date', 'assigned_to']
        widgets = {
            'due_date': DateTimeInput(attrs={'type': 'datetime-local'}),
        }

class CommentForm(forms.ModelForm):
    class Meta():
        model = Comment
        fields = ['author', 'event', 'task', 'content']

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['phone_number', 'role', 'user']
        widgets = {
            'user': forms.TextInput(attrs={'readonly': True}),
            'role': forms.Select(attrs={'disabled': True}),
        }

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['user'].required = False
        self.fields['role'].required = False

    def save(self, commit=True):
        instance = super(UserProfileForm, self).save(commit=False)
        if commit:
            instance.save()
        return instance

class NotificationForm(forms.ModelForm):
    class Meta():
        model = Notification
        fields = ['user', 'message', 'is_read']

class ScheduleForm(forms.ModelForm):
    class Meta():
        model = Schedule
        fields = ['user', 'events']
