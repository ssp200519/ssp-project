# Generated by Django 5.0.4 on 2024-05-13 00:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stations', '0005_userprofile_email_userprofile_first_name_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='email',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='last_name',
        ),
    ]
