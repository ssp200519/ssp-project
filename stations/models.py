from django.db import models
from django.contrib.auth.models import User
from django.utils.formats import date_format
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20, blank=True)
    role = models.CharField(max_length=30, choices=[('student', 'Student'), ('employee', 'Employee')], default='student')

    def __str__(self):
        return self.user.username


class Event(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    organizer = models.ForeignKey(UserProfile, related_name='organized_events', on_delete=models.SET_NULL, null=True)
    participants = models.ManyToManyField(UserProfile, related_name='events_attending')

    def __str__(self):
        start_str = self.start_time.strftime("%d.%m.%Y %H:%M")
        end_str = self.end_time.strftime("%d.%m.%Y %H:%M")
        organizer_str = self.organizer.user.username if self.organizer else 'No organizer'
        return f"{self.name} (Start: {start_str}, End: {end_str}) by {organizer_str}"

class Task(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    due_date = models.DateTimeField()
    assigned_to = models.ManyToManyField(UserProfile, related_name='tasks')

    def __str__(self):
        due_date_str = date_format(self.due_date, "SHORT_DATETIME_FORMAT")
        assigned_to_names = ', '.join([user.user.username for user in self.assigned_to.all()])
        return f'{self.title} (Due: {due_date_str}) - Assigned to: {assigned_to_names}'

class Comment(models.Model):
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, related_name='comments', on_delete=models.CASCADE, null=True, blank=True)
    task = models.ForeignKey(Task, related_name='comments', on_delete=models.CASCADE, null=True, blank=True)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Comment by {self.author} on {self.created_at}'

class Schedule(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    events = models.ManyToManyField(Event)

    def __str__(self):
        return f'Schedule for {self.user}'

class Notification(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    message = models.CharField(max_length=255)
    is_read = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        display_message = ('"'+self.message[:27] + '..."' if len(self.message) > 30 else '"'+self.message+'"')
        return f'Notification for {self.user} - {"Read" if self.is_read else "Unread"}: {display_message}'